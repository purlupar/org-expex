;; org-expex - Copyright (c) 2020-2021 David Diem

(defun org-latex-filter-gloss (text backend info)
  (when (org-export-derived-backend-p backend 'latex)
    (replace-regexp-in-string "\\(gloss}\\|^$\\)\n\\(.+\\)\n\\(.+\\)\n\\(.+\\)"
			      "\\1\n\\\\a\n\\\\begingl\n\\\\gla \\2 //\n\\\\glb \\3 //\n\\\\glft \\4 //\n\\\\endgl" text)))

(defun org-latex-filter-pex (text backend info)
  (when (org-export-derived-backend-p backend 'latex)
    (replace-regexp-in-string "\\(\\\\begin{gloss}\\)\\(\\(.*\n\\)\\{9,\\}\\)\\(\\\\end{gloss}\\)"
			      "\\\\pex[*=*]\\2\\\\xe" text
			      )))

(defun org-latex-filter-ex (text backend info)
  (when (org-export-derived-backend-p backend 'latex)
    (replace-regexp-in-string "\\\\begin{gloss}\n\\(\\\\label{.*}\\)?\n?\\(\\\\a\\)\n?\\(\\(.*\n\\)\\{,7\\}\\)\\\\\\(end{gloss}\\)"
			      "\\\\ex\n\\1\\3\\\\xe"
			      text
			      )))

(defun org-latex-filter-gloss-clean (text backend info)
  (when (org-export-derived-backend-p backend 'latex)
    (replace-regexp-in-string "\\\\gl.+ = //\n"
			      "" text
			      )))
(defun org-latex-filter-gloss-group (text backend info)
  (when (org-export-derived-backend-p backend 'latex)
    (replace-regexp-in-string "\\\\{" "{"
			      (replace-regexp-in-string
			       "\\\\}" "}" text))))

(defun org-latex-filter-gloss-star (text backend info)
  (when (org-export-derived-backend-p backend 'latex)
    (replace-regexp-in-string "\\(\\\\gla \\)\\(\*\\)"
			      "\\1\\\\ljudge*" text)))

(defun org-latex-filter-gloss-comment (text backend info)
  (when (org-export-derived-backend-p backend 'latex)
    (replace-regexp-in-string "\\(\\\\gla \\)\\(.*\\):: \\(.*\\) //"
			      "\\1\\\\rightcomment{\\3}\\2 //" text)))

(defun org-latex-filter-gloss-label (text backend info)
  (when (org-export-derived-backend-p backend 'latex)
    (replace-regexp-in-string "\\(\\\\a\\)\\(\\(.*\n\\)*\\)\\(\\\\label{.*}\\)"
			      "\\4\n\\1\\2" text)))

(defun org-latex-filter-gloss-save-title (text backend info)
  (when (org-export-derived-backend-p backend 'latex)
    (replace-regexp-in-string "\\(\\\\begin{gloss}\n\\)\\(.*\n\\)\n"
			      "\\2\\1" text)))

(defun org-latex-filter-gloss-insert-title (text backend info)
  (when (org-export-derived-backend-p backend 'latex)
    (replace-regexp-in-string "\\(.*\n\\)\\(\\\\p?ex\\(\\[\\*=\\*\\]\\)?\n\\)"
			      "\\2\\1" text)))



(setq org-export-filter-special-block-functions
      '(
	org-latex-filter-gloss-save-title
	org-latex-filter-gloss
	org-latex-filter-gloss-group	
	org-latex-filter-gloss-star
	org-latex-filter-gloss-comment
	org-latex-filter-gloss-label	
	org-latex-filter-gloss-clean
	org-latex-filter-ex
	org-latex-filter-pex
	org-latex-filter-gloss-insert-title	
	))
